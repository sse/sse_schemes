#include "cxt.hpp"

#include <type_traits>



namespace sse
{
namespace sks
{


	static_assert(std::is_same<SimpleSksClient::keyword_type, GsvClient::keyword_type>::value, "Keyword types must be equal.");
	static_assert(std::is_same<SimpleSksClient::index_type, GsvClient::index_type>::value, "Index types must be equal.");
	static_assert(std::is_same<SimpleSksClient::index_type, CrossTagClient::index_type>::value, "Index types must be equal.");



CxtClient::CxtClient(sse::dbparser::DBParser &parser) :
sks_client_(parser, true), sks_verifier_(parser), cross_tag_client_(parser)
{
}
CxtClient::~CxtClient()
{
}

CxtClient::encrypted_database_type CxtClient::finalize_setup()
{
	encrypted_database_type edb;
	
	edb.sks_edb = sks_client_.finalize_setup();
	edb.gsv_edb = sks_verifier_.finalize_setup();
	edb.xt_edb  = cross_tag_client_.finalize_setup();
	
	return edb;
}


CxtClient::sk_search_token_type CxtClient::single_keyword_search(const keyword_type& keyword) const
{
	sk_search_token_type token;
	
	token.sks_token = sks_client_.search_trapdoor(keyword);
	token.gsv_token = sks_verifier_.keyword_label(keyword);
	
	return token;
}

std::list<CxtClient::index_type> CxtClient::decrypt_check_intermediate_results(const keyword_type& keyword, const intermediate_result_type& intermediate_results) const
{
	std::list<index_type> dec_results = sks_client_.decrypt_search_results(keyword, intermediate_results.indices);
	
	sks_verifier_.check_results(keyword, dec_results, intermediate_results.proof);
	
	return dec_results;
}


CxtClient::xt_search_token_type CxtClient::cross_tags(const std::list<index_type>& intermediate_results, const std::list<keyword_type>& keywords) const
{	
	return cross_tag_client_.filter_tokens(intermediate_results, keywords);
}

std::list<CxtClient::index_type> CxtClient::filter_results(const std::list<index_type>& indices, const std::list<keyword_type>& keywords, const filters_type& filters) const
{
	return cross_tag_client_.filter_results(indices, keywords, filters);
}

CxtClient::add_token_type CxtClient::add_token(const index_type& document, const std::list<keyword_type>& keywords)
{
	CxtClient::add_token_type add_token;
	auto tmp = sks_client_.add_token(document,keywords);
	add_token.sks_token = tmp.first;
	sks_add_tmp_state_ = tmp.second;
	
	add_token.gsv_token = sks_verifier_.add_token(document,keywords);
	add_token.xt_token 	= cross_tag_client_.add_token(document,keywords);
	
	return add_token;
}

void CxtClient::check_update_add(const add_token_type& add_tokens, add_response_type& response)
{
	sks_verifier_.check_add(add_tokens.gsv_token, response.gsv_response);
	
	cross_tag_client_.check_add(add_tokens.xt_token, response.xt_response);
	
	sks_client_.increment_counters(sks_add_tmp_state_, response.sks_response);
	sks_add_tmp_state_.clear();
}


CxtClient::del_token_type CxtClient::delete_token(const index_type& document, const std::list<keyword_type>& keywords) const
{
	CxtClient::del_token_type del_token;
	del_token.sks_token = sks_client_.delete_token(document,keywords);
	del_token.gsv_token = sks_verifier_.delete_token(document,keywords);
	del_token.xt_token 	= cross_tag_client_.delete_token(document,keywords);
	
	return del_token;
}

void CxtClient::check_update_del(const del_token_type& del_tokens, del_response_type& response)
{
	sks_verifier_.check_del(del_tokens.gsv_token, response.gsv_response);
	
	cross_tag_client_.check_del(del_tokens.xt_token, response.xt_response);
}


CxtServer::CxtServer(CxtClient::Edb &&edb) :
sks_server_(std::move(edb.sks_edb)), sks_prover_(std::move(edb.gsv_edb)), cross_tag_server_(std::move(edb.xt_edb))
{
	
}
CxtServer::~CxtServer()
{
}

CxtClient::intermediate_result_type CxtServer::keyword_search(const CxtClient::sk_search_token_type& token) const
{
	CxtClient::intermediate_result_type r;
	
	r.indices = sks_server_.search(token.sks_token);
	r.proof = sks_prover_.get_proof(token.gsv_token);
	
	return r;
}

CxtClient::filters_type CxtServer::get_filters(const CxtClient::xt_search_token_type& cross_token) const
{
	return cross_tag_server_.retrieve(cross_token);
}


CxtClient::add_response_type CxtServer::add(const CxtClient::add_token_type& add_token)
{
	CxtClient::add_response_type add_response;

	add_response.sks_response = sks_server_.add(add_token.sks_token);;
	
	add_response.gsv_response = sks_prover_.add(add_token.gsv_token);
	add_response.xt_response  = cross_tag_server_.add(add_token.xt_token);
	
	return add_response;
}

CxtClient::del_response_type CxtServer::del(const CxtClient::del_token_type& del_token)
{
	CxtClient::del_response_type del_response;

	sks_server_.revoke(del_token.sks_token);
	
	del_response.gsv_response = sks_prover_.del(del_token.gsv_token);
	del_response.xt_response  = cross_tag_server_.del(del_token.xt_token);
	
	return del_response;
}

}

}