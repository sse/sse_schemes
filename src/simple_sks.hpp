#pragma once

#include <sse/crypto/prf.hpp>
#include <sse/crypto/cipher.hpp>
#include <sse/crypto/fpe.hpp>
#include <sse/dbparser/DBParser.h>

#include <cstdint>

#include <list>
#include <unordered_set>
#include <unordered_map>
#include <set>
#include <map>
#include <string>
#include <vector>
#include <memory>

namespace sse
{

namespace sks
{

class SimpleSksClient
{
public:
	
	/* TYPES */
	struct SearchToken;
	struct AddToken;
	
    typedef std::string		keyword_type;
	typedef unsigned		index_type;


    typedef uint64_t		label_type;
	static constexpr unsigned kLabelSize = sizeof(label_type); // 64 bits labels 
	// it means we cannot have more than 2^32 documents in the database without risking collisions (birthday paradox)

	typedef std::string 	encrypted_value_type;

	
	typedef uint64_t		revoked_label_type;
	
	typedef std::unordered_map<SimpleSksClient::label_type, SimpleSksClient::encrypted_value_type> Edb;
	typedef Edb 			encrypted_database_type;
	
	typedef uint32_t		counter_type; 
	// it is useless to have counters of size more than sizeof(label_type)/2 because of the birthday paradox


	typedef SearchToken 	search_token_type;
	typedef AddToken 		add_element_type;

	typedef std::list<add_element_type>
							add_token_type;
	typedef std::vector<bool>
							add_response_type;
	
	typedef std::unordered_set<revoked_label_type> 
							del_token_type;
	
	typedef void			del_response_type;
	
	static constexpr unsigned kDerivedKeysSize = 16;
	// 128 bits keys for PRFs
	
	
	/* DECLARATIONS */
	
	SimpleSksClient(sse::dbparser::DBParser &parser, bool encrypt_results = false);
	~SimpleSksClient();
	
	bool encrypt_results() const;
	
	std::unique_ptr<encrypted_database_type> finalize_setup();
	search_token_type search_trapdoor(const keyword_type& keyword) const;
	
	 std::list<index_type> decrypt_search_results(const keyword_type& keyword, const std::list<index_type>& enc_results) const;
	
	std::pair<add_token_type, std::list<keyword_type> > add_token(const index_type& document, const std::list<keyword_type>& keywords) const;
	void increment_counters(const std::list<keyword_type>& keywords, const add_response_type& increment_mask);
	
	del_token_type delete_token(const index_type& document, const std::list<keyword_type>& keywords) const;
	
private:
	
	void encrypt_list(const std::string& keyword, const std::list<unsigned> &documents);
	
	bool is_setup_;
	const bool encrypt_results_;
	
	encrypted_database_type *edb_;
	sse::crypto::Prf<kDerivedKeysSize + sse::crypto::Cipher::kKeySize> master_prf_;
	sse::crypto::Prf<kDerivedKeysSize + sse::crypto::Cipher::kKeySize> add_prf_;
	sse::crypto::Prf<kDerivedKeysSize> del_prf_;
	
	sse::crypto::Prf<sse::crypto::Fpe::kKeySize> pfe_key_gen_;
	
	std::map<keyword_type, counter_type> count_dictionary_;
};

struct SimpleSksClient::SearchToken
{
	std::string K1;
	std::string K2;
	std::string K1_add;
	std::string K2_add;
	std::string K_del;
};

struct SimpleSksClient::AddToken
{
	SimpleSksClient::label_type label;
	std::string enc_id;
	SimpleSksClient::revoked_label_type rev_id;
	
	AddToken(const SimpleSksClient::label_type& l, std::string& e, SimpleSksClient::revoked_label_type& r) : label(l), enc_id(e), rev_id(r)
		{};
};



class SimpleSksServer
{
public:
	typedef SimpleSksClient::Edb 			encrypted_database_type;
	typedef SimpleSksClient::index_type		index_type;

	SimpleSksServer(std::unique_ptr<encrypted_database_type> &&edb);
	~SimpleSksServer();
	
	std::list<index_type> search(const SimpleSksClient::search_token_type& token) const; 
	
	SimpleSksClient::add_response_type add(const SimpleSksClient::add_token_type& add_token);
	SimpleSksClient::del_response_type revoke(const SimpleSksClient::del_token_type& del_token);
	
private:
	// Start with the static part. Declare it as const.
	const std::unique_ptr<encrypted_database_type> edb_; 
	
	// Now the dynamic part
	std::map<SimpleSksClient::label_type, SimpleSksClient::encrypted_value_type> add_dict_; 
	std::set<SimpleSksClient::revoked_label_type> revocation_set_;
};


}
}