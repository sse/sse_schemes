#include <boost/test/unit_test.hpp>

#include "simple_sks.hpp"
#include "gsv.hpp"

#include <sse/dbparser/DBParserJSON.h>


using namespace sse::sks ;

static void print_list(const list<string> &l)
{
	for(auto it = l.begin(); it != l.end(); ++it)
	{
		if(it != l.begin())
		{
			cout << ", ";
		}
		cout << *it;
	}
}

static void print_list(const list<unsigned> &l)
{
	for(auto it = l.begin(); it != l.end(); ++it)
	{
		if(it != l.begin())
		{
			cout << ", ";
		}
		cout << *it;
	}
}

BOOST_AUTO_TEST_SUITE(SingleKeyword)
	
std::list<sse::sks::SimpleSksServer::index_type> search(sse::sks::SimpleSksClient &client, GsvClient &client_verif, sse::sks::SimpleSksServer& server, GsvServer &server_verif, const std::string& keyword)
{
	auto search_token = client.search_trapdoor(keyword);
	auto verif_label = client_verif.keyword_label(keyword);
	
	auto results = server.search(search_token);
	
	if(client.encrypt_results())
	{
		results = client.decrypt_search_results(keyword, results);
	}
	
	auto search_proof = server_verif.get_proof(verif_label);
	
	client_verif.check_results(keyword, results, search_proof);
	
	return results;
}

void add_list(sse::sks::SimpleSksClient &client, GsvClient &client_verif, sse::sks::SimpleSksServer& server, GsvServer &server_verif, const std::list<std::string>& keywords, const sse::sks::SimpleSksClient::index_type& doc)
{
	auto add_token_pair =  client.add_token(doc, keywords);
	auto add_verif_token = client_verif.add_token(doc, keywords);
	
	auto bitset = server.add(add_token_pair.first);
	auto add_proof = server_verif.add(add_verif_token);

	client_verif.check_add(add_verif_token, add_proof);
	
	client.increment_counters(add_token_pair.second, bitset);
}

void revoke(sse::sks::SimpleSksClient &client, GsvClient &client_verif, sse::sks::SimpleSksServer& server, GsvServer &server_verif, const std::list<std::string>& keywords, const sse::sks::SimpleSksClient::index_type& doc)
{
	auto del_token =  client.delete_token(doc, keywords);
	auto del_verif_token =  client_verif.delete_token(doc, keywords);

	server.revoke(del_token);
	auto del_proof = server_verif.del(del_verif_token);

	client_verif.check_del(del_verif_token, del_proof);
	
}


void test_sks(const char *filename, bool encrypt_results)
{    
    sse::dbparser::DBParserJSON parser(filename);
	sse::sks::SimpleSksClient client(parser, encrypt_results);
	sse::sks::GsvClient client_verif(parser);
	
	parser.parse();


	std::unique_ptr<sse::sks::SimpleSksClient::encrypted_database_type> edb = client.finalize_setup();
	std::unique_ptr<sse::sks::GsvClient::verifiable_map_type> vht = client_verif.finalize_setup();
		
	sse::sks::SimpleSksServer server(std::move(edb));
	sse::sks::GsvServer server_verif(std::move(vht));

	
	string keyword_1 = "shoot";
	string keyword_2 = "igualada";
	
	auto results = search(client, client_verif, server, server_verif, keyword_1);
	std::list<sse::sks::SimpleSksServer::index_type> expected_results({12, 13});
	BOOST_REQUIRE(results == expected_results);

	add_list(client, client_verif, server, server_verif, {keyword_1}, 17);

	results = search(client, client_verif, server, server_verif, keyword_1);
	expected_results = std::list<sse::sks::SimpleSksServer::index_type>({12, 13, 17});
	BOOST_REQUIRE(results == expected_results);

	revoke(client, client_verif, server, server_verif, {keyword_1}, 17);

	results = search(client, client_verif, server, server_verif, keyword_1);
	expected_results = std::list<sse::sks::SimpleSksServer::index_type>({12, 13});
	BOOST_REQUIRE(results == expected_results);


	add_list(client, client_verif, server, server_verif, {keyword_1}, 17);

	results = search(client, client_verif, server, server_verif, keyword_1);
	expected_results = std::list<sse::sks::SimpleSksServer::index_type>({12, 13, 17});
	BOOST_REQUIRE(results == expected_results);


	revoke(client, client_verif, server, server_verif, {keyword_1}, 12);

	results = search(client, client_verif, server, server_verif, keyword_1);
	expected_results = std::list<sse::sks::SimpleSksServer::index_type>({13, 17});
	BOOST_REQUIRE(results == expected_results);

	add_list(client, client_verif, server, server_verif, {keyword_1}, 12);

	results = search(client, client_verif, server, server_verif, keyword_1);
	expected_results = std::list<sse::sks::SimpleSksServer::index_type>({12, 13, 17});
	BOOST_REQUIRE(results == expected_results);


	revoke(client, client_verif, server, server_verif, {keyword_1, keyword_2}, 13);

	results = search(client, client_verif, server, server_verif, keyword_1);
	expected_results = std::list<sse::sks::SimpleSksServer::index_type>({12, 17});
	BOOST_REQUIRE(results == expected_results);

	results = search(client, client_verif, server, server_verif, keyword_2);
	expected_results = std::list<sse::sks::SimpleSksServer::index_type>({12, 14});
	BOOST_REQUIRE(results == expected_results);

	add_list(client, client_verif, server, server_verif, {keyword_2, keyword_1}, 25);

	results = search(client, client_verif, server, server_verif, keyword_1);
	
	expected_results = std::list<sse::sks::SimpleSksServer::index_type>({12, 17, 25});
	BOOST_REQUIRE(results == expected_results);

	results = search(client, client_verif, server, server_verif, keyword_2);	
	expected_results = std::list<sse::sks::SimpleSksServer::index_type>({12, 14, 25});
	BOOST_REQUIRE(results == expected_results);

	string keyword_3 = "new";
	
	add_list(client, client_verif, server, server_verif, {keyword_3}, 25);

	results = search(client, client_verif, server, server_verif, keyword_3);
	expected_results = std::list<sse::sks::SimpleSksServer::index_type>({25});
	BOOST_REQUIRE(results == expected_results);

	revoke(client, client_verif, server, server_verif, {keyword_3}, 25);

	results = search(client, client_verif, server, server_verif, keyword_3);
	expected_results = std::list<sse::sks::SimpleSksServer::index_type>();
	BOOST_REQUIRE(results == expected_results);

	add_list(client, client_verif, server, server_verif, {keyword_3}, 25);

	results = search(client, client_verif, server, server_verif, keyword_3);
	expected_results = std::list<sse::sks::SimpleSksServer::index_type>({25});
	BOOST_REQUIRE(results == expected_results);
}

BOOST_AUTO_TEST_CASE(sks_unencrypted) {
	const char* filename = "/Users/raphaelbost/Code/sse/sks/inverted_index_docs.txt";
	test_sks(filename, false);
}

BOOST_AUTO_TEST_CASE(sks_encrypted) {
	const char* filename = "inverted_index_docs.txt";
	test_sks(filename, true);
}

BOOST_AUTO_TEST_SUITE_END()
