#include <boost/test/unit_test.hpp>

#include "cxt.hpp"

#include <sse/dbparser/DBParserJSON.h>

using namespace sse::sks;

std::list<sse::sks::CxtClient::index_type> search(sse::sks::CxtClient &client, sse::sks::CxtServer& server, const std::list<CxtClient::keyword_type>& keywords)
{
	assert(keywords.size() > 0);

	// First round
	auto search_token = client.single_keyword_search(keywords.front());

	auto intermediate_results = server.keyword_search(search_token);

	intermediate_results.indices = client.decrypt_check_intermediate_results(keywords.front(), intermediate_results);
	
	if(keywords.size() == 1)
	{
		return intermediate_results.indices;
	}
	
	// Second round
	
	const std::list<std::string> x_terms(++keywords.begin(), keywords.end());

	auto cross_tags = client.cross_tags(intermediate_results.indices, x_terms);

	auto filters = server.get_filters(cross_tags);

	auto results = client.filter_results(intermediate_results.indices, x_terms, filters);
	
	return results;
}

void add(sse::sks::CxtClient &client, sse::sks::CxtServer& server, const std::list<CxtClient::keyword_type>& keywords, const sse::sks::CxtClient::index_type& doc)
{
	auto add_token = client.add_token(doc, keywords);
	
	auto add_response = server.add(add_token);
	
	client.check_update_add(add_token, add_response);
}

void del(sse::sks::CxtClient &client, sse::sks::CxtServer& server, const std::list<CxtClient::keyword_type>& keywords, const sse::sks::CxtClient::index_type& doc)
{
	auto del_token = client.delete_token(doc, keywords);
	
	auto del_response = server.del(del_token);
	
	client.check_update_del(del_token, del_response);
}

BOOST_AUTO_TEST_SUITE(MultiKeyword)


BOOST_AUTO_TEST_CASE(cxt) {
	const char* filename = "inverted_index_docs.txt";

    sse::dbparser::DBParserJSON parser(filename);
	sse::sks::CxtClient client(parser);

	parser.parse();

	sse::sks::CxtServer server(std::move(client.finalize_setup()));

	string keyword_1 = "shoot";
	string keyword_2 = "igualada";
	string keyword_3 = "everi";
	string keyword_4 = "strikebreak";

	BOOST_REQUIRE( search(client, server, {keyword_1}) == std::list<unsigned>( {12, 13} ));
	BOOST_REQUIRE( search(client, server, {keyword_2}) == std::list<unsigned>( {12, 13, 14} ));
	BOOST_REQUIRE( search(client, server, {keyword_3}) == std::list<unsigned>( {13} ));
	BOOST_REQUIRE( search(client, server, {keyword_4}) == std::list<unsigned>( {14} ));
	
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_2}) == std::list<unsigned>( {12, 13} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_3}) == std::list<unsigned>( {13} ));
	BOOST_REQUIRE( search(client, server, {keyword_2, keyword_3}) == std::list<unsigned>( {13} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_2, keyword_3}) == std::list<unsigned>( {13} ));
	BOOST_REQUIRE( search(client, server, {keyword_2, keyword_1, keyword_3}) == std::list<unsigned>( {13} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_4, keyword_3}) == std::list<unsigned>() );
	
	add(client, server, {keyword_1}, 14);
	BOOST_REQUIRE( search(client, server, {keyword_1}) == std::list<unsigned>( {12, 13, 14} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_2}) == std::list<unsigned>( {12, 13, 14} ));
	BOOST_REQUIRE( search(client, server, {keyword_2, keyword_3}) == std::list<unsigned>( {13} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_2, keyword_3}) == std::list<unsigned>( {13} ));
	BOOST_REQUIRE( search(client, server, {keyword_2, keyword_1, keyword_3}) == std::list<unsigned>( {13} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_4, keyword_3}) == std::list<unsigned>() );

	add(client, server, {keyword_3, keyword_2}, 25);
	BOOST_REQUIRE( search(client, server, {keyword_1}) == std::list<unsigned>( {12, 13, 14} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_2}) == std::list<unsigned>( {12, 13, 14} ));
	BOOST_REQUIRE( search(client, server, {keyword_2, keyword_3}) == std::list<unsigned>( {13, 25} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_2, keyword_3}) == std::list<unsigned>( {13} ));
	BOOST_REQUIRE( search(client, server, {keyword_2, keyword_1, keyword_3}) == std::list<unsigned>( {13} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_4, keyword_3}) == std::list<unsigned>( ) );


	del(client, server, {keyword_3, keyword_2, keyword_4, keyword_1}, 13);
	BOOST_REQUIRE( search(client, server, {keyword_1}) == std::list<unsigned>( {12, 14} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_2}) == std::list<unsigned>( {12, 14} ));
	BOOST_REQUIRE( search(client, server, {keyword_2, keyword_3}) == std::list<unsigned>( {25} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_2, keyword_3}) == std::list<unsigned>( {} ));
	BOOST_REQUIRE( search(client, server, {keyword_2, keyword_1, keyword_3}) == std::list<unsigned>( {} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_4, keyword_3}) == std::list<unsigned>( ) );

	add(client, server, {keyword_2, keyword_4, keyword_1}, 27);
	BOOST_REQUIRE( search(client, server, {keyword_1}) == std::list<unsigned>( {12, 14, 27} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_2}) == std::list<unsigned>( {12, 14, 27} ));
	BOOST_REQUIRE( search(client, server, {keyword_2, keyword_3}) == std::list<unsigned>( {25} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_2, keyword_3}) == std::list<unsigned>( {} ));
	BOOST_REQUIRE( search(client, server, {keyword_2, keyword_1, keyword_3}) == std::list<unsigned>( {} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_4, keyword_3}) == std::list<unsigned>( ) );

	add(client, server, {keyword_2, keyword_1}, 29);
	BOOST_REQUIRE( search(client, server, {keyword_1}) == std::list<unsigned>( {12, 14, 27, 29} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_2}) == std::list<unsigned>( {12, 14, 27, 29} ));
	BOOST_REQUIRE( search(client, server, {keyword_2, keyword_3}) == std::list<unsigned>( {25} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_2, keyword_3}) == std::list<unsigned>( {} ));
	BOOST_REQUIRE( search(client, server, {keyword_2, keyword_1, keyword_3}) == std::list<unsigned>( {} ));
	BOOST_REQUIRE( search(client, server, {keyword_1, keyword_4, keyword_3}) == std::list<unsigned>( ) );

}

BOOST_AUTO_TEST_SUITE_END()
