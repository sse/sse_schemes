#include "simple_sks.hpp"

#include <sse/crypto/cipher.hpp>

#include <cstring>
#include <iostream>
#include <iomanip>

namespace sse
{

namespace sks
{


SimpleSksClient::SimpleSksClient(sse::dbparser::DBParser &parser, bool encrypt_results) :
is_setup_(false), encrypt_results_(encrypt_results), edb_(new Edb()), master_prf_(), add_prf_(), del_prf_(), pfe_key_gen_(), count_dictionary_()
{
    parser.addCallbackList(std::bind(&SimpleSksClient::encrypt_list,this,std::placeholders::_1, std::placeholders::_2));
	
}

SimpleSksClient::~SimpleSksClient()
{
}

bool SimpleSksClient::encrypt_results() const
{
	return encrypt_results_;
}

void SimpleSksClient::encrypt_list(const std::string& keyword, const std::list<unsigned> &documents)
{
	assert(!is_setup_);
		
	// derive a key from the keyword and the master key
	std::array<uint8_t, kDerivedKeysSize + sse::crypto::Cipher::kKeySize> K;
	K = master_prf_.prf(keyword);
	
	sse::crypto::Prf<kLabelSize> label_prf(K.data(),kDerivedKeysSize);
	std::array<uint8_t, sse::crypto::Cipher::kKeySize> cipher_key;
	std::copy_n(K.begin()+kDerivedKeysSize, sse::crypto::Cipher::kKeySize, cipher_key.begin());
	
	sse::crypto::Fpe pre_encryption(pfe_key_gen_.prf(keyword));
	sse::crypto::Cipher id_cipher(cipher_key);
	
	label_type label;
	std::string id_string, encrypted_id;

	id_string.resize(sizeof(unsigned));
		
	counter_type c = 0;
	for(auto it = documents.begin(); it != documents.end(); ++it, ++c)
	{
		auto label_array = label_prf.prf((unsigned char *) &c,sizeof(counter_type));
		memcpy(&label, label_array.data(), sizeof(label_type));
		
		index_type doc = *it;
		if(encrypt_results_)
		{
			doc = pre_encryption.encrypt(doc);
		}
		
		// could be a good idea to change that and use Fpe (less space used, same security ...)
		id_string = std::string((char*)&(doc), sizeof(unsigned));
		
		
		id_cipher.encrypt(id_string,encrypted_id);

		
		(*edb_)[label] = encrypted_id;
	}
}

std::unique_ptr<SimpleSksClient::encrypted_database_type> SimpleSksClient::finalize_setup()
{
	is_setup_ = true;
	return std::unique_ptr<encrypted_database_type>(edb_);
}

SimpleSksClient::search_token_type SimpleSksClient::search_trapdoor(const keyword_type& keyword) const
{
	assert(is_setup_);
	search_token_type trap;
	
	std::array<uint8_t, kDerivedKeysSize + sse::crypto::Cipher::kKeySize> K;
	K = master_prf_.prf(keyword);

	trap.K1 = std::string(K.begin(),K.begin()+kDerivedKeysSize);
	trap.K2 = std::string(K.begin()+kDerivedKeysSize,K.end());
	
	K = add_prf_.prf(keyword);
	
	trap.K1_add = std::string(K.begin(),K.begin()+kDerivedKeysSize);
	trap.K2_add = std::string(K.begin()+kDerivedKeysSize,K.end());
	
	std::array<uint8_t, kDerivedKeysSize> K_del;
	
	K_del = del_prf_.prf(keyword);
	
	trap.K_del = std::string(K_del.begin(),K_del.begin()+kDerivedKeysSize);
	
	return trap;
}


std::list<SimpleSksClient::index_type> SimpleSksClient::decrypt_search_results(const keyword_type& keyword, const std::list<index_type>& enc_results) const
{
	assert(is_setup_);
	assert(encrypt_results_);
	
	sse::crypto::Fpe pre_encryption(pfe_key_gen_.prf(keyword));
	std::list<SimpleSksClient::index_type> results;
	
	std::string tmp;
	index_type id;
	
	for(auto r = enc_results.begin(); r != enc_results.end(); ++r)
	{
		tmp = std::string((char *)&(*r), sizeof(index_type));
		tmp = pre_encryption.decrypt(tmp);
		
		// back to index_type
		assert(tmp.length() == sizeof(SimpleSksServer::index_type));

		memcpy(&id, tmp.data(), sizeof(SimpleSksServer::index_type));
		
		results.push_back(id);
	}
	
	
	return results;
}

std::pair<SimpleSksClient::add_token_type, std::list<SimpleSksClient::keyword_type> > SimpleSksClient::add_token(const index_type& document, const std::list<keyword_type>& keywords) const
{
	assert(is_setup_);
	
	typedef std::pair<add_element_type, keyword_type> temp_token_element_type;
	std::list<temp_token_element_type> temp_token;
	
	std::array<uint8_t, kDerivedKeysSize + sse::crypto::Cipher::kKeySize> K_add;
	std::array<uint8_t, kDerivedKeysSize> K_del;
	label_type label;
	std::string base_id_string, encrypted_id;
	revoked_label_type rev_id;
	
	std::string id_string;
	
	for(auto it = keywords.begin(); it != keywords.end() ; ++it)
	{
		K_add = add_prf_.prf(*it);
		K_del = del_prf_.prf(*it);

        sse::crypto::Prf<sizeof(label_type)> label_prf(K_add.data(),kDerivedKeysSize);
		sse::crypto::Cipher id_cipher((uint8_t*) K_add.data()+kDerivedKeysSize);
		sse::crypto::Prf<sizeof(revoked_label_type)> rev_prf(K_del.data(),kDerivedKeysSize);
		sse::crypto::Fpe pre_encryption(pfe_key_gen_.prf(*it));
		
		counter_type count = 0;
		if(count_dictionary_.count(*it)>0)
		{
			count = count_dictionary_.at(*it);
		}
		
		index_type doc = document;
		
		if(encrypt_results_)
		{
			doc = pre_encryption.encrypt(doc);
		}
		
		id_string = std::string((char*)&(doc), sizeof(unsigned));
		
				
		auto label_array = label_prf.prf((unsigned char *) &count,sizeof(counter_type));
		id_cipher.encrypt(id_string,encrypted_id);
		auto rev_id_array = rev_prf.prf((unsigned char *) id_string.data(),sizeof(index_type));
		
        static_assert(sizeof(label) == label_array.size(), "Invalid size");
        static_assert(sizeof(revoked_label_type) == rev_id_array.size(), "Invalid size");

        memcpy(&label, label_array.data(), sizeof(label));
        memcpy(&rev_id, rev_id_array.data(), sizeof(revoked_label_type));
        
		add_element_type elt = add_element_type(label, encrypted_id, rev_id);
		temp_token.push_back(std::make_pair(elt, *it));
		
	}
	// sort according to the label
	
	auto comp = [](const temp_token_element_type& x, const temp_token_element_type& y)
	{
		return (x.first.label) < (y.first.label);
	};
	temp_token.sort(comp);
	
	add_token_type token;
	std::list<keyword_type> keyword_list;
	
	while(!temp_token.empty())
	{
		// use move constructors to avoid extraneous memory
		temp_token_element_type tmp = std::move( temp_token.front() );
		temp_token.pop_front();

		token.push_back(std::move(tmp.first));
		keyword_list.push_back(std::move(tmp.second));

	}
	
	return std::make_pair(token, keyword_list);
}

void SimpleSksClient::increment_counters(const std::list<keyword_type>& keywords, const add_response_type& increment_mask)
{
	assert(keywords.size() == increment_mask.size());
	
	
	auto it_k = keywords.begin();
	auto it_b = increment_mask.begin();
	
	for( ; it_k != keywords.end(); ++it_k, ++it_b)
	{
		if(*it_b == 0)
		{
			if(count_dictionary_.count(*it_k)==0)
			{
				count_dictionary_[*it_k] = 1;
			}else{
				count_dictionary_[*it_k]++;
			}
		}
	}
}

SimpleSksClient::del_token_type SimpleSksClient::delete_token(const index_type& document, const std::list<keyword_type>& keywords) const
{
	assert(is_setup_);
	
	del_token_type token;
	token.reserve(keywords.size());
	
	std::array<uint8_t, kDerivedKeysSize> K1_del;

    revoked_label_type rev_id;
	
	for(auto it = keywords.begin(); it != keywords.end() ; ++it)
	{
		K1_del = del_prf_.prf(*it);
		sse::crypto::Prf<sizeof(revoked_label_type)> rev_prf(K1_del.data(),kDerivedKeysSize);
		sse::crypto::Fpe pre_encryption(pfe_key_gen_.prf(*it));
		
        index_type d = document;
		if(encrypt_results_)
		{
			d = pre_encryption.encrypt(d);
		}
		
        auto rev_id_array = rev_prf.prf((unsigned char *) &d,sizeof(index_type));
        static_assert(sizeof(revoked_label_type) == rev_id_array.size(), "Invalid size");
        memcpy(&rev_id, rev_id_array.data(), sizeof(revoked_label_type));
        

		token.insert(rev_id);
	}

	return token;
}


SimpleSksServer::SimpleSksServer(std::unique_ptr<encrypted_database_type> &&edb) :
edb_(std::move(edb))
{
	
}

SimpleSksServer::~SimpleSksServer()
{
}

std::list<SimpleSksServer::index_type> SimpleSksServer::search(const SimpleSksClient::search_token_type& token) const
{
	std::list<SimpleSksServer::index_type> results, tmp_list;
	
	sse::crypto::Prf<SimpleSksClient::kLabelSize> label_prf(token.K1);
	sse::crypto::Cipher id_cipher((uint8_t*)token.K2.data());
	
	SimpleSksClient::label_type label;
	std::string id_string = "1234", encrypted_id;
	SimpleSksServer::index_type id;
	
	// first use the static keys
	for(SimpleSksClient::counter_type c = 0; ; ++c)
	{
        auto label_array = label_prf.prf((unsigned char *) &c,sizeof(SimpleSksClient::counter_type));
        static_assert(sizeof(label) == label_array.size(), "Invalid size");
        memcpy(&label, label_array.data(), sizeof(label));

		if(edb_->count(label) == 0)
		{
			break;
		}
		
		encrypted_id = edb_->at(label);
 		// decrypt the id
		id_cipher.decrypt(encrypted_id, id_string);
		
		assert(id_string.length() == sizeof(SimpleSksServer::index_type));

		memcpy(&id, id_string.data(), sizeof(SimpleSksServer::index_type));

		tmp_list.push_back(id);
	}
	
	// now look for pairs added after setup
	
	sse::crypto::Prf<SimpleSksClient::kLabelSize> add_label_prf(token.K1_add);
	sse::crypto::Cipher add_id_cipher((uint8_t*)token.K2_add.data());
	
	for(SimpleSksClient::counter_type c = 0; ; ++c)
    {
        auto label_array = add_label_prf.prf((unsigned char *) &c,sizeof(SimpleSksClient::counter_type));
        static_assert(sizeof(label) == label_array.size(), "Invalid size");
        memcpy(&label, label_array.data(), sizeof(label));

		auto it = add_dict_.find(label);
		if(it == add_dict_.end())
		{
			break;
		}
		
		encrypted_id = it->second;
		// decrypt the id
		add_id_cipher.decrypt(encrypted_id, id_string);
		
		// convert to the right type
		assert(id_string.length() == sizeof(SimpleSksServer::index_type));

		memcpy(&id, id_string.data(), sizeof(SimpleSksServer::index_type));

		tmp_list.push_back(id);		
	}
	
	// now check the revocation set
	sse::crypto::Prf<sizeof(SimpleSksClient::revoked_label_type)> rev_prf(token.K_del);
	SimpleSksClient::revoked_label_type rev_id;
	
	for(auto it = tmp_list.begin(); it != tmp_list.end(); ++it)
	{
        auto rev_id_array = rev_prf.prf((unsigned char *) &(*it),sizeof(SimpleSksServer::index_type));
        static_assert(sizeof(SimpleSksClient::revoked_label_type) == rev_id_array.size(), "Invalid size");
        memcpy(&rev_id, rev_id_array.data(), sizeof(SimpleSksClient::revoked_label_type));

		if(revocation_set_.count(rev_id) == 0) // id is not in the revocation set
		{
			results.push_back(*it);
		}
	}
	
	return results;
}

SimpleSksClient::add_response_type SimpleSksServer::add(const SimpleSksClient::add_token_type& add_token)
{
	std::vector<bool> bitset;
	for(auto it = add_token.begin(); it != add_token.end(); ++it)
	{

		auto rev_it = revocation_set_.find(it->rev_id);
		
		if(rev_it == revocation_set_.end())
		{
			// the element has not been previously revoked
			add_dict_[it->label] = it->enc_id;
			bitset.push_back(false);
		}else{
			// the element has been revoked
			// remove it from the revocation set
			revocation_set_.erase(rev_it);
			bitset.push_back(true);
		}
	}
	
	return bitset;
}

SimpleSksClient::del_response_type SimpleSksServer::revoke(const SimpleSksClient::del_token_type& del_token)
{
	// go through the del token (list) and add everything to the revocation set
	revocation_set_.insert(del_token.begin(), del_token.end());
}

}
}