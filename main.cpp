#include <iostream>
#include <iomanip>
#include <string>
#include <array>
#include <vector>
#include <chrono>

#include "src/simple_sks.hpp"
#include "src/gsv.hpp"
#include "src/cross_tags.hpp"

#include <sse/dbparser/DBParserJSON.h>

using namespace std;

void print_list(const list<string> &l)
{
	for(auto it = l.begin(); it != l.end(); ++it)
	{
		if(it != l.begin())
		{
			cout << ", ";
		}
		cout << *it;
	}
}

void test_gsv(const char *filename) {

	
    sse::dbparser::DBParserJSON parser(filename);
	sse::sks::SimpleSksClient client(parser);
	sse::sks::GsvClient client_verif(parser);
		
	cout << "Started parsing\n";
	
	parser.parse();
	
	cout << "Finished parsing\n";
	
	std::unique_ptr<sse::sks::SimpleSksClient::encrypted_database_type> edb = client.finalize_setup();
	std::unique_ptr<sse::sks::GsvClient::verifiable_map_type> vht = client_verif.finalize_setup();
	
	cout << "Setup finalized\n";
	
	sse::sks::SimpleSksServer server(std::move(edb));
	sse::sks::GsvServer server_verif(std::move(vht));

	cout << "Server initialized\n";
	
	string keyword = "shoot";
	auto search_token = client.search_trapdoor(keyword);
	auto verif_label = client_verif.keyword_label(keyword);

	cout << "Search token generated\n";
	
	auto results = server.search(search_token);
	auto search_proof = server_verif.get_proof(verif_label);
		
	cout << "Check search results: ";
	
	try{
		client_verif.check_results(keyword, results, search_proof);
	}
	catch(std::exception &e)
	{
		cout << "INVALID PROOF:\n";
		cout << e.what() << endl;
	}
		
	cout << "Search completed\n";

	cout << "Search results for keyword \"" << keyword << "\":\n";
	cout << "[";
	for(auto it = results.begin(); it != results.end(); ++it)
	{
		if(it != results.begin())
		{
			cout << ", ";
		}
		cout << (int) *it;
	}
	cout << "]\n";
	
	cout << "Add document\n";
	cout << "Generate token\n";
	auto add_token_pair =  client.add_token(17, std::list<std::string>({"shoot"}));
	auto add_verif_token = client_verif.add_token(17, std::list<std::string>({"shoot"}));
	
	cout << "Tell server to add\n";
	auto bitset = server.add(add_token_pair.first);
	auto add_proof = server_verif.add(add_verif_token);
	
		
	cout << "Update client's state\n";
	client.increment_counters(add_token_pair.second, bitset);
	try{
		client_verif.check_add(add_verif_token, add_proof);
	}
	catch(std::exception &e)
	{
		cout << "INVALID UPDATE PROOF:\n";
		cout << e.what() << endl;
	}

	keyword = "shoot";
	search_token = client.search_trapdoor(keyword);
	verif_label = client_verif.keyword_label(keyword);

	cout << "Search token generated\n";
	
	results = server.search(search_token);
	search_proof = server_verif.get_proof(verif_label);

	cout << "Check search results: ";
	
	try{
		client_verif.check_results(keyword, results, search_proof);
	}
	catch(std::exception &e)
	{
		cout << "INVALID PROOF:\n";
		cout << e.what() << endl;
	}
	
	cout << "Search completed\n";

	cout << "Search results for keyword \"" << keyword << "\":\n";
	cout << "[";
	for(auto it = results.begin(); it != results.end(); ++it)
	{
		if(it != results.begin())
		{
			cout << ", ";
		}
		cout << (int) *it;
	}
	cout << "]\n";


	cout << "Delete document\n";
	cout << "Generate token\n";
	auto del_token =  client.delete_token(17, std::list<std::string>({"shoot"}));
	auto del_verif_token =  client_verif.delete_token(17, std::list<std::string>({"shoot"}));
		
	cout << "Tell server to delete\n";
	server.revoke(del_token);
	auto del_proof = server_verif.del(del_verif_token);

	cout << "Update client's state\n";
	
	try{
		client_verif.check_del(del_verif_token, del_proof);
	}
	catch(std::exception &e)
	{
		cout << "INVALID UPDATE PROOF:\n";
		cout << e.what() << endl;
	}
	

	keyword = "shoot";
	search_token = client.search_trapdoor(keyword);
	verif_label = client_verif.keyword_label(keyword);

	cout << "Search token generated\n";
	
	results = server.search(search_token);
	search_proof = server_verif.get_proof(verif_label);

	cout << "Check search results: ";
	
	try{
		client_verif.check_results(keyword, results, search_proof);
	}
	catch(std::exception &e)
	{
		cout << "INVALID PROOF:\n";
		cout << e.what() << endl;
	}

	cout << "Search completed\n";

	cout << "Search results for keyword \"" << keyword << "\":\n";
	cout << "[";
	for(auto it = results.begin(); it != results.end(); ++it)
	{
		if(it != results.begin())
		{
			cout << ", ";
		}
		cout << (int) *it;
	}
	cout << "]\n";

	cout << "Re-Add document\n";
	cout << "Generate token\n";
	add_token_pair =  client.add_token(25, std::list<std::string>({"shoot"}));
	add_verif_token = client_verif.add_token(17, std::list<std::string>({"shoot"}));

	cout << "Tell server to add\n";
	bitset = server.add(add_token_pair.first);
	add_proof = server_verif.add(add_verif_token);

	cout << "Update client's state\n";
	client.increment_counters(add_token_pair.second, bitset);
	
	try{
		client_verif.check_add(add_verif_token, add_proof);
	}
	catch(std::exception &e)
	{
		cout << "INVALID UPDATE PROOF:\n";
		cout << e.what() << endl;
	}

	keyword = "shoot";
	search_token = client.search_trapdoor(keyword);
	verif_label = client_verif.keyword_label(keyword);

	cout << "Search token generated\n";
	
	results = server.search(search_token);
	search_proof = server_verif.get_proof(verif_label);

	cout << "Check search results: ";
	
	try{
		client_verif.check_results(keyword, results, search_proof);
	}
	catch(std::exception &e)
	{
		cout << "INVALID PROOF:\n";
		cout << e.what() << endl;
	}
	
	cout << "Search completed\n";

	cout << "Search results for keyword \"" << keyword << "\":\n";
	cout << "[";
	for(auto it = results.begin(); it != results.end(); ++it)
	{
		if(it != results.begin())
		{
			cout << ", ";
		}
		cout << (int) *it;
	}
	cout << "]\n";

}

void test_cxt(const char *filename) {
	
    sse::dbparser::DBParserJSON parser(filename);
	
	sse::sks::SimpleSksClient client(parser);
	sse::sks::GsvClient client_verif(parser);
	sse::sks::CrossTagClient cxt_client(parser);
		
	cout << "Started parsing\n";
	
	parser.parse();
	
	cout << "Finished parsing\n";
	
	std::unique_ptr<sse::sks::SimpleSksClient::encrypted_database_type> edb = client.finalize_setup();
	std::unique_ptr<sse::sks::GsvClient::verifiable_map_type> vht = client_verif.finalize_setup();
	std::unique_ptr<sse::sks::CrossTagClient::verifiable_set_type> e_set = cxt_client.finalize_setup();
	
	cout << "Setup finalized\n";
	
	sse::sks::SimpleSksServer server(std::move(edb));
	sse::sks::GsvServer server_verif(std::move(vht));
	sse::sks::CrossTagServer cxt_server(std::move(e_set));

	cout << "Server initialized\n";
	
	string keyword = "shoot";
	auto search_token = client.search_trapdoor(keyword);
	auto verif_label = client_verif.keyword_label(keyword);

	cout << "Search token for first keyword generated\n";
	
	auto results = server.search(search_token);
	auto search_proof = server_verif.get_proof(verif_label);
		
	cout << "Check search results: ";
	
	try{
		client_verif.check_results(keyword, results, search_proof);
	}
	catch(std::exception &e)
	{
		cout << "INVALID PROOF:\n";
		cout << e.what() << endl;
	}
	
	cout << "First keyword Search completed\n";
	
	cout << "Search results for keyword \"" << keyword << "\":\n";
	cout << "[";
	for(auto it = results.begin(); it != results.end(); ++it)
	{
		if(it != results.begin())
		{
			cout << ", ";
		}
		cout << (int) *it;
	}
	cout << "]\n";

	cout << "Filter\n";
	
	list<string> filter_keys = {"igualada"};
	auto filters = cxt_client.filter_tokens(results,filter_keys);

	cout << "Filter tokens generated for keywords: {";
	print_list(filter_keys);
	cout << "}\n";
		
	auto response = cxt_server.retrieve(filters);
	
	cout << "Server responded\n";
	
	auto filtered_results = cxt_client.filter_results(results, filter_keys, response);

	cout << "Filtered results: [";
	for(auto it = filtered_results.begin(); it != filtered_results.end(); ++it)
	{
		if(it != filtered_results.begin())
		{
			cout << ", ";
		}
		cout << (int) *it;
	}
	cout << "]\n";


	filter_keys = {"toto"};
	filters = cxt_client.filter_tokens(results,filter_keys);

	cout << "Filter tokens generated for keywords: {";
	print_list(filter_keys);
	cout << "}\n";
	response = cxt_server.retrieve(filters);

	cout << "Server responded\n";

	filtered_results = cxt_client.filter_results(results, filter_keys, response);

	cout << "Filtered results: [";
	for(auto it = filtered_results.begin(); it != filtered_results.end(); ++it)
	{
		if(it != filtered_results.begin())
		{
			cout << ", ";
		}
		cout << (int) *it;
	}
	cout << "]\n";
	
	cout << "Add pair\n";
	
	auto add_token = cxt_client.add_token(12, {"toto"});
	
	auto insertion_proofs = cxt_server.add(add_token);
	
	try{
		cxt_client.check_add(add_token, insertion_proofs);
	}
	catch(std::exception &e)
	{
		cout << "INVALID CROSS TAG PROOF:\n";
		cout << e.what() << endl;
	}
	
	filter_keys = {"toto"};
	filters = cxt_client.filter_tokens(results,filter_keys);

	cout << "Filter tokens generated for keywords: {";
	print_list(filter_keys);
	cout << "}\n";
	response = cxt_server.retrieve(filters);

	cout << "Server responded\n";

	filtered_results = cxt_client.filter_results(results, filter_keys, response);

	cout << "Filtered results: [";
	for(auto it = filtered_results.begin(); it != filtered_results.end(); ++it)
	{
		if(it != filtered_results.begin())
		{
			cout << ", ";
		}
		cout << (int) *it;
	}
	cout << "]\n";


	auto del_token = cxt_client.delete_token(12, {"toto"});
	
	auto deletion_proofs = cxt_server.del(del_token);
	
	try{
		cxt_client.check_del(del_token, deletion_proofs);
	}
	catch(std::exception &e)
	{
		cout << "INVALID CROSS TAG PROOF:\n";
		cout << e.what() << endl;
	}
	
	filter_keys = {"toto"};
	filters = cxt_client.filter_tokens(results,filter_keys);

	cout << "Filter tokens generated for keywords: {";
	print_list(filter_keys);
	cout << "}\n";
	response = cxt_server.retrieve(filters);

	cout << "Server responded\n";

	filtered_results = cxt_client.filter_results(results, filter_keys, response);

	cout << "Filtered results: [";
	for(auto it = filtered_results.begin(); it != filtered_results.end(); ++it)
	{
		if(it != filtered_results.begin())
		{
			cout << ", ";
		}
		cout << (int) *it;
	}
	cout << "]\n";
	
	
}
int main( int argc, char* argv[] ) {
	
    const char *filename;
    
    
    if(argc != 2) {
        filename = "inverted_index_docs.txt";
    } else {
        filename = argv[1];
    }
	
	test_cxt(filename);
	// test_gsv(filename);
	
	return 0;
}