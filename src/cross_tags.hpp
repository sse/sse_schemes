#pragma once

#include <sse/crypto/prf.hpp>
#include <sse/dbparser/DBParser.h>
#include <sse/verifiable_containers/verifiable_set.hpp>


#include <string>
#include <memory>

namespace sse
{
namespace sks
{

class CrossTagClient
{

public:    
	typedef std::string		keyword_type;
	typedef unsigned		index_type;
	
	typedef std::string cross_tag_type;
	static constexpr unsigned kCrossTagSize = 8; // 64 bits cross tags -> 2^32 pairs before probable collision
	typedef std::list<std::list<cross_tag_type>> filter_token_type;
	
	
	typedef sse::verifiable_containers::verifiable_set<cross_tag_type> verifiable_set_type;
	
	typedef std::list<std::list<verifiable_set_type::membership_proof_type>> response_type;
	
	typedef std::string		add_element_type;
	typedef std::list<add_element_type>
							add_token_type;
	typedef std::list<verifiable_set_type::insertion_proof>
							add_response_type;
	

	typedef std::string		del_element_type;
	typedef std::list<del_element_type>
							del_token_type;
	typedef std::list<verifiable_set_type::deletion_proof>
							del_response_type;
	
	CrossTagClient(sse::dbparser::DBParser &parser);
	~CrossTagClient();
	
	std::unique_ptr<verifiable_set_type> finalize_setup();
	
	cross_tag_type cross_tag(const index_type& index, const keyword_type& keyword) const;
	std::list<cross_tag_type> cross_tag_list(const index_type& index, const std::list<keyword_type>& keywords) const;
	filter_token_type filter_tokens(const std::list<index_type>& indices, const std::list<keyword_type>& keywords) const;
	
	void check_results(const std::string& keyword, const index_type& index, const verifiable_set_type::membership_proof_type& proof) const;
	bool match(const std::list<verifiable_set_type::membership_proof_type>& proofs) const;
	std::list<index_type> filter_results(const std::list<index_type>& indices, const std::list<keyword_type>& keywords, const response_type& response) const;
	
	
	add_token_type add_token(const index_type& document, const std::list<keyword_type>& keywords) const;
	void check_add(const add_token_type& add_token, add_response_type& insertion_proofs);

	del_token_type delete_token(const index_type& document, const std::list<keyword_type>& keywords) const;
	void check_del(const del_token_type& del_token, del_response_type& deletion_proofs);
	
	 
private:
	void encrypt_pair(const std::string& keyword, const unsigned &doc);
	
	bool is_setup_;
	
	
	verifiable_set_type *e_set_;
	verifiable_set_type::digest_type digest_;
	
	sse::crypto::Prf<kCrossTagSize> tag_prf_;
	
};

class CrossTagServer
{
public:
	typedef CrossTagClient::verifiable_set_type verifiable_set_type;
	typedef CrossTagClient::cross_tag_type cross_tag_type;
	
	CrossTagServer(std::unique_ptr<verifiable_set_type> &&e_set);
	~CrossTagServer();
	
	CrossTagClient::response_type retrieve(const CrossTagClient::filter_token_type& filter_tokens) const;
	
	CrossTagClient::add_response_type add(const CrossTagClient::add_token_type& add_token);
	CrossTagClient::del_response_type del(const CrossTagClient::del_token_type& del_token);
	
	
private:
	
	std::unique_ptr<verifiable_set_type> e_set_;
};

}
}